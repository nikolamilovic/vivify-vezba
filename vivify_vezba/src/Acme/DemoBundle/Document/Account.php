<?php

namespace Acme\DemoBundle\Document;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @MongoDB\Document(repositoryClass="Acme\DemoBundle\Documents\AccountRepository")
 */
class Account implements UserInterface
{
    protected $first_name;
    protected $last_name;
    protected $email;
    protected $password;
    /**
     * @var MongoId $id
     */
    protected $id;


    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return self
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;
        return $this;
    }

    /**
     * Get firstName
     *
     * @return string $firstName
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return self
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;
        return $this;
    }

    /**
     * Get lastName
     *
     * @return string $lastName
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Get password
     *
     * @return string $password
     */
    public function getPassword()
    {
        return $this->password;
    }

    public function getSalt()
    {
        return null;
    }

    public function getRoles()
    {
        return array('ROLE_ADMIN');
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function eraseCredentials()
    {

    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('first_name', new NotBLank());
        $metadata->addPropertyConstraint('first_name',  new Assert\Regex(array(
            'pattern' => '/\d/',
            'match' => false,
            'message' => 'Your name cannot contain a number'
        )));

        $metadata->addPropertyConstraint('last_name', new NotBlank());
        $metadata->addPropertyConstraint('last_name',  new Assert\Regex(array(
            'pattern' => '/\d/',
            'match' => false,
            'message' => 'Your name cannot contain a number'
        )));

        $metadata->addPropertyConstraint('email', new NotBlank());
        $metadata->addPropertyConstraint('email', new Assert\Email(array(
            'message' => 'The email {{ value }} is not a valid email.',
            'checkMX' => true,
        )));

        $metadata->addPropertyConstraint('password', new NotBlank());
        $metadata->addPropertyConstraint('password', new Assert\Length(array(
            'min' => 6,
            'minMessage' => 'Your password must be at least {{ limit }} characters long.'
        )));
        $metadata->addPropertyConstraint('password', new Assert\Regex(array(
            'pattern' => '/\d/',
            'match' => true,
            'message' => 'You password must contain at least one digit.'
        )));
    }
}
