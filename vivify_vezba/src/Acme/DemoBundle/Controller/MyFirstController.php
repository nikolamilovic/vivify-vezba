<?php

namespace Acme\DemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MyFirstController extends Controller
{
    public function indexAction()
    {
        return $this->render('AcmeDemoBundle:MyFirst:index.html.twig');
    }

    public function hiAction($name)
    {
        return $this->render('AcmeDemoBundle:MyFirst:hi.html.twig', array(
            'name' => $name
        ));
    }
}