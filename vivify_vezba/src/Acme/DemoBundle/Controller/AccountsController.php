<?php

namespace Acme\DemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\DemoBundle\Entity\Accounts;
use Symfony\Component\HttpFoundation\Response;

class AccountsController extends Controller
{
    public function listAction()
    {
        $accounts = $this->getDoctrine()
                    ->getRepository('AcmeDemoBundle:Accounts')
                    ->findAll();

        if (!$accounts) {
            throw $this->createNotFoundException('No accounts found.');
        }

        return $this->render('AcmeDemoBundle:Accounts:list.html.twig', array('accounts' => $accounts));
    }

    public function infoAction($id)
    {
        $account = $this->getDoctrine()
                    ->getRepository('AcmeDemoBundle:Accounts')
                    ->find($id);

        if (!$account) {
            throw $this->createNotFoundException('Nothing found for account ID ' . $id);
        }

        return $this->render('AcmeDemoBundle:Accounts:info.html.twig', array('account' => $account));
    }

    public function createAction()
    {
        $account = new Accounts();
        $account->setFirstName('Petar');
        $account->setLastName('Petrovic');
        $account->setEmail('petar@petrovic.rs');
        $account->setPassword('petrovasifra');

        $em = $this->getDoctrine()->getManager();

        $em->persist($account);
        $em->flush();

        return new Response('Created an account with ID ' . $account->getId());
    }

    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $account = $em->getRepository('AcmeDemoBundle:Accounts')->find($id);

        if (!$account) {
            throw $this->createNotFoundException('Account not found.');
        }

        $account->setFirstName($account->getFirstName() . ' Updated');
        $em->flush();

        return new Response('Account has been updated.');
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $account = $em->getRepository('AcmeDemoBundle:Accounts')->find($id);

        if (!$account) {
            throw $this->createNotFoundException('Account not found.');
        }

        $em->remove($account);
        $em->flush();

        return new Response($account->getFirstName() . '\'s account has been deleted.');
    }
}
