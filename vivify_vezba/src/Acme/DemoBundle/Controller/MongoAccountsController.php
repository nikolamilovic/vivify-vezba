<?php

namespace Acme\DemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\DemoBundle\Document\Account;
use Symfony\Component\HttpFoundation\Response;

class MongoAccountsController extends Controller
{
    public function listAction()
    {
        $accounts = $this->get('doctrine_mongodb')
                        ->getRepository('AcmeDemoBundle:Account')
                        ->findAll();

        if (!$accounts) {
            throw $this->createNotFoundException('No accounts found');
        }

        return $this->render('AcmeDemoBundle:MongoAccounts:list.html.twig', array('accounts' => $accounts));
    }

    public function infoAction($id)
    {
        $account = $this->get('doctrine_mongodb')
                        ->getRepository('AcmeDemoBundle:Account')
                        ->find($id);

        if (!$account) {
            throw $this->createNotFoundException('Account not found');
        }

        return $this->render('AcmeDemoBundle:MongoAccounts:info.html.twig', array('account' => $account));
    }

    public function createAction()
    {
        $account = new Account();
        $account->setFirstName('John');
        $account->setLastName('Doe');
        $account->setEmail('john@doe.com');
        $account->setPassword('sifra');

        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->persist($account);
        $dm->flush();

        return new Response('Create a new account with ID ' . $account->getId());
    }
}