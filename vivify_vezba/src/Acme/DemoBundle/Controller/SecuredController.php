<?php

namespace Acme\DemoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Acme\DemoBundle\Document\Account;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

/**
 * @Route("/demo/secured")
 */
class SecuredController extends Controller
{
    /**
     * @Route("/login", name="_demo_login")
     * @Template()
     */
    public function loginAction(Request $request)
    {
        $securityContext = $this->container->get('security.context');

        if ($securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirect($this->generateUrl('_demo_secured_hello'));
        }

        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $request->getSession()->get(SecurityContext::AUTHENTICATION_ERROR);
        }

        return array(
            'last_username' => $request->getSession()->get(SecurityContext::LAST_USERNAME),
            'error'         => $error,
        );
    }

    /**
     * @Route("/login_check", name="_security_check")
     */
    public function securityCheckAction()
    {
        // The security layer will intercept this request
    }

    /**
     * @Route("/logout", name="_demo_logout")
     */
    public function logoutAction()
    {
        // The security layer will intercept this request
    }

    /**
     * @Route("/hello", defaults={"name"="World"}),
     * @Route("/hello/{name}", name="_demo_secured_hello")
     * @Template()
     */
    public function helloAction($name='')
    {
        $securityContext = $this->container->get('security.context');

        if (!$securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirect($this->generateUrl('_demo_login'));
        }

        $user = $this->get('security.context')->getToken()->getUser();

        return array('name' => $user->getFirstName() . ' ' . $user->getLastName());
    }

    /**
     * @Route("/hello/admin/{name}", name="_demo_secured_hello_admin")
     * @Template()
     */
    public function helloadminAction($name)
    {
        return array('name' => $name);
    }

    public function registerAction(Request $request)
    {
        $securityContext = $this->container->get('security.context');

        if ($securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirect($this->generateUrl('_demo_secured_hello'));
        }

        $account = new Account();

        $form = $this
                ->createFormBuilder($account)
                ->add('first_name', 'text', array('attr' => array('class' => 'form-control')))
                ->add('last_name', 'text', array('attr' => array('class' => 'form-control')))
                ->add('email', 'email', array('attr' => array('class' => 'form-control')))
                ->add('password', 'repeated', array(
                    'type' => 'password',
                    'invalid_message' => 'The password fields must match.',
                    'options' => array('attr' => array('class' => 'form-control')),
                    'required' => true,
                    'first_options' => array('label' => 'Password'),
                    'second_options' => array('label' => 'Repeat Password')
                ))
                ->add('save', 'submit', array('label' => 'Register', 'attr' => array('class' => 'btn btn-block btn-primary')))
                ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $account = $form->getData();

            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->persist($account);
            $dm->flush();

            $token = new UsernamePasswordToken($account, $account->getPassword(), 'default', $account->getRoles());
            $this->get('security.context')->setToken($token);

            return $this->redirect($this->generateUrl('_demo_secured_hello'));
        }

        return $this->render('AcmeDemoBundle:Secured:register.html.twig', array('form' => $form->createView()));
    }
}
