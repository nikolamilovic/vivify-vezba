<?php

namespace Acme\DemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\DemoBundle\Entity\Accounts;
use Acme\DemoBundle\Document\Account;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;


class AdminController extends Controller
{
    public function indexAction()
    {

        return $this->render('AcmeDemoBundle:Admin:index.html.twig');
    }

    public function loginFormAction()
    {
        return $this->render('AcmeDemoBundle:Admin:login.html.twig');
    }

    public function loginCheckAction()
    {
        $request = Request::createFromGlobals();

        $account = $this->get('doctrine_mongodb')
            ->getRepository('AcmeDemoBundle:Account')
            ->findOneByEmail($request->get('email'));

        if (!$account) {
            throw new UsernameNotFoundException('User not found.');
        } else {
            //$token = new UsernamePasswordToken($account, )
        }

        if ($account->getPassword()!=$request->get('password')) {
            return $this->render('AcmeDemoBundle:Admin:login.html.twig', array('error'=>'Wrong password'));
            return new Response('Wrong password');
        }


        if ($account->getPassword()!=$request->get('password')) {
            return new Response('Wrong password');
        }



        return new Response('Checking...' . $request->get('email'));
    }

}